/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      colors: {
        blue: {
          900: '#131E44',
          30: '#457b9d'
        },
        red: {
          100: '#FFEBED',
          900: '#9F2937'
        },
        slate: {
          50: '#FFEBEF',
          300: '#E6E6E6'
        },
        green: {
          50: '#c0d4d3',
          100: '#2d8984',
        }
      }
    },
    fontFamily: {
      'display': ['"Abhaya Libre"', 'serif'],
      'body': ['"Montserrat"', 'sans-serif'],
    },
    screens: {
      '2xs': '300px',
      'xs': '600px',
      'sm': '768px',
      'md': '960px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    }
  },
  plugins: [],
}